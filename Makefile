.PHONY: clean-build deb release helz-docker

VERSION_NO=$(shell grep -e "^version" Cargo.toml | sed -e 's/version = //')
BUILD_NO=1
GIT_DESCRIBE=$(shell git describe --always --tag --dirty | sed s/[v]//g)

build/debian/helz-linux-x86_64: helz-linux-x86_64
	@install -m 0755 ./helz-linux-x86_64 ./build/debian/helz-linux-x86_64

build/debian/helz.1: docs/helz.1
	@install -m 0644 docs/helz.1 build/debian/helz.1

build/debian/NEWS:
	@./build/news.sh ${VERSION_NO} > build/debian/NEWS

build/el7/rpmbuild/NEWS:
	@./build/news.sh ${VERSION_NO} > build/el7/rpmbuild/NEWS

build/el7/rpmbuild/helz.1: docs/helz.1
	@install -m 0644 docs/helz.1 build/el7/rpmbuild/helz.1

deb: build/debian/helz-linux-x86_64 build/debian/NEWS build/debian/helz.1
	@cd build/debian; make helz_${VERSION_NO}-${BUILD_NO}_amd64.deb VERSION_NO=${VERSION_NO} BUILD_NO=${BUILD_NO}

docs/helz.adoc: docs/helz.adoc.in
	./docs/generate-helz.sh ${VERSION_NO} > docs/helz.adoc

docs/helz.1: docs/helz.adoc
	@asciidoctor --backend manpage docs/helz.adoc --out-file docs/helz.1

helz: $(wildcard src/*.rs src/*/*.rs)
	cargo build --release --locked
	cp ./target/release/helz helz

helz-linux-x86_64: $(wildcard src/*.rs src/*/*.rs)
	if [ -z "${CI}" ]; then docker run -v $$(pwd):/app -w /app rust:latest cargo build --release --locked; fi
	if [ -z "${CI}" ]; then cp ./target/release/helz helz-linux-x86_64; fi

release:
	       cargo check
	       @echo "releasing v${VERSION_NO}"
	       @if [[ ${GIT_DESCRIBE} == *"-dirty" ]]; then echo "will not release a dirty version"; exit 1; fi
	       @if [[ $$(git tag -l | grep v${VERSION_NO}) ]]; then echo "release for v${VERSION_NO} already exists"; exit 1; fi
	       @echo "finding release notes for v${VERSION_NO}"
	       ./build/news.sh ${VERSION_NO}
	       @echo "making release for v${VERSION_NO}";
	       @git tag -a v${VERSION_NO} -m v${VERSION_NO}
	       @git push
	       @git push --tags

rpm: build/el7/rpmbuild/NEWS build/el7/rpmbuild/helz.1
	cd build/el7; make rpmbuild/SPECS/helz.spec VERSION_NO=${VERSION_NO} BUILD_NO=${BUILD_NO}
	docker build -f build/el7/Dockerfile . --build-arg HELZ_VERSION=${VERSION_NO} --build-arg BUILD_NO=${BUILD_NO}

README.adoc: helz
	@# replace example codeblock with generated example config from Helz.
	@awk < README.adoc > README.adoc.tmp ' \
		/\.Helz configuration example/ && found_helz_example_codeblock == 0 { \
			found_helz_example_codeblock = 1; \
		} \
		/^----$$/ && found_helz_example_codeblock == 1 && in_helz_example_codeblock == 0 { \
			found_helz_example_codeblock = 2; \
			in_helz_example_codeblock = 1; \
			print; \
			next; \
		} \
		/^----$$/ && in_helz_example_codeblock == 1 { \
			in_helz_example_codeblock = 0; \
			system("./helz show-config"); \
		} \
		in_helz_example_codeblock == 1 { next; } \
		{ print; }'
	@mv README.adoc.tmp README.adoc

clean-build:
	git clean -dfX build
