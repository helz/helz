#!/bin/sh

set -e

VERSION="$1"

awk < docs/NEWS.rst -v VERSION="Helz v${VERSION}" '
# Look for heading line matching the desired version
$0 == VERSION {
	is_current_version = 1
	# Skip this line so it doesnt match
	# against the check for other version lines
	next
}

# Find the next line containing a version number
# and mark that so we can stop printing
/^Helz v([0-9]+\.){2}[0-9](-[0-9a-zA-Z]*)?$/ {
	is_current_version = 0
}

# Print as long as we have passed the current version line
# and we have some content
# and we dont have a line with only ===s
is_current_version == 1 && /.+/ && ! /^=+/ {
	print
}
'
