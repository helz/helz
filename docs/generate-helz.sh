#!/bin/sh

set -e

VERSION_NO="$1"

awk < docs/helz.adoc.in \
	-vVERSION_NO="${VERSION_NO:-"0.0.0"}" \
	'
/XXVERSIONXX/ {
	print "v" VERSION_NO
	next
}

{print}
'
