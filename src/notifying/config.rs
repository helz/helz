/*! The overall program config of all notifiers.
 */

use serde::{Deserialize, Serialize};
use std::convert::From;
use std::time::{Duration, Instant};

use crate::config::traits::Example;
use crate::notifying::discord::{DiscordNotifier, DiscordNotifierConfig};
use crate::notifying::opsgenie::{OpsGenie, OpsGenieConfig};
use crate::notifying::slack::{Slack, SlackConfig};
use crate::notifying::stdout::{StdoutNotifier, StdoutNotifierConfig};
use crate::notifying::traits::Notifier;
use crate::notifying::webex::{WebexNotifier, WebexNotifierConfig};
use crate::polling::config::HelzCheckStatus;

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
/// The overarching struct of Helz notifiers.
pub struct HelzNotificationConfig {
    /// Timeout specified in seconds.
    /// Use [timeout](crate::notifying::config::HelzNotificationConfig::timeout) instead.
    #[deprecated]
    pub timeout_s: Option<u64>,
    /// Configure how long to wait before notifying.
    /// Default is 0, as in notify immediately.
    #[serde(default)]
    #[serde(with = "humantime_serde")]
    pub timeout: Option<Duration>,
    #[deprecated]
    /// Interval specified in seconds.
    /// Use [interval](crate::notifying::config::HelzNotificationConfig::interval) instead.
    pub interval_s: Option<u64>,
    /// Configure how often to notify.
    /// Default is 0, which means to notify for as long as it is failing.
    #[serde(default)]
    #[serde(with = "humantime_serde")]
    pub interval: Option<Duration>,
    /// The configuration for a stdout notifier.
    pub stdout: Option<StdoutNotifierConfig>,
    /// The configuration for a discord notifier.
    pub discord: Option<DiscordNotifierConfig>,
    /// The configuration for an OpsGenie notifier.
    pub opsgenie: Option<OpsGenieConfig>,
    /// The configuration for a slack notifier.
    pub slack: Option<SlackConfig>,
    /// The configuration for a webex notifier.
    pub webex: Option<WebexNotifierConfig>,
}

/// The runtime struct for notifiers.
pub struct HelzNotification {
    /// A list of configured notifiers.
    pub notifiers: Vec<Box<dyn Notifier + Send + Sync>>,
    /// How long to wait before issuing the first notification.
    pub timeout: Option<Duration>,
    /// How often to send notifications.
    pub interval: Duration,
    /// When the last notification was sent.
    pub last_notification: Option<Instant>,
}

impl From<HelzNotificationConfig> for HelzNotification {
    fn from(conf: HelzNotificationConfig) -> Self {
        let mut notifiers = Vec::new();

        if let Some(stdout_config) = conf.stdout {
            let stdout =
                Box::new(StdoutNotifier::from(stdout_config)) as Box<dyn Notifier + Send + Sync>;
            notifiers.push(stdout);
        }

        if let Some(config) = conf.discord {
            let notifier =
                Box::new(DiscordNotifier::from(config)) as Box<dyn Notifier + Send + Sync>;
            notifiers.push(notifier);
        }

        if let Some(config) = conf.opsgenie {
            let notifier = Box::new(OpsGenie::from(config)) as Box<dyn Notifier + Send + Sync>;
            notifiers.push(notifier);
        }

        if let Some(config) = conf.slack {
            let notifier = Box::new(Slack::from(config)) as Box<dyn Notifier + Send + Sync>;
            notifiers.push(notifier);
        }

        if let Some(config) = conf.webex {
            let notifier = Box::new(WebexNotifier::from(config)) as Box<dyn Notifier + Send + Sync>;
            notifiers.push(notifier);
        }

        let mut interval = conf.interval.unwrap_or_else(|| Duration::from_secs(5));

        // Keep backwards compatability with 'interval_s'.
        #[allow(deprecated)]
        if let Some(interval_s) = conf.interval_s {
            warn!("The configuration parameter 'interval_s' has been deprecated. Use 'interval' instead.");
            if conf.interval.is_some() {
                warn!("Both 'interval' and 'interval_s' are configured. 'interval' will take precedence.");
            } else {
                interval = Duration::from_secs(interval_s);
            }
        }

        let mut timeout = conf.timeout;

        // Keep backwards compatability with 'timeout_s'.
        #[allow(deprecated)]
        if let Some(timeout_s) = conf.timeout_s {
            warn!("The configuration parameter 'timeout_s' has been deprecated. Use 'timeout' instead.");
            if conf.timeout.is_some() {
                warn!("Both 'timeout' and 'timeout_s' are configured. 'timeout' will take precedence.");
            } else {
                timeout = Some(Duration::from_secs(timeout_s));
            }
        }
        HelzNotification {
            notifiers,
            timeout,
            interval,
            last_notification: None,
        }
    }
}

impl HelzNotification {
    /// This function is called by [Polling](crate::polling::config::Polling)
    /// when it receives an error during a health check.
    pub async fn notify(&mut self, errors: &[HelzCheckStatus], whoami: &str) {
        let last_notification = self.last_notification;
        for notifier in &self.notifiers {
            let should_notify = match last_notification {
                Some(last) => last.elapsed() > self.interval,
                None => {
                    if let Some(timeout) = self.timeout {
                        debug!(
                            "timeout for notify, notify should happen {:?} after first error",
                            timeout
                        );
                        if let Some(first_error) = errors.first() {
                            first_error.time.elapsed() > timeout
                        } else {
                            // No errors, no notify.
                            false
                        }
                    } else {
                        true
                    }
                }
            };
            trace!(
                "should notify: {}, {:?} since last notify, config says {:?}",
                should_notify,
                self.last_notification,
                self.timeout
            );
            if should_notify {
                trace!("stdout notifying");
                notifier.notify(errors, whoami).await;
                self.last_notification = Some(Instant::now());
            }
        }
    }

    /// This function is called by [Polling](crate::polling::config::Polling)
    /// when it receives OK after having previously received an error during
    /// a health check.
    pub async fn ok(&mut self, errors: &[HelzCheckStatus], whoami: &str) {
        let last_notification = self.last_notification;
        self.last_notification = None;

        // Don't notify about OK if we never notified.
        // This can happen if a timeout for initial
        // error is configured.
        if last_notification.is_none() {
            return;
        }

        for notifier in &self.notifiers {
            notifier.ok(errors, whoami).await;
        }
    }
}

impl Example for HelzNotificationConfig {
    fn example() -> Self {
        #[allow(deprecated)]
        Self {
            interval: Some(Duration::from_secs(300)),
            timeout: Some(Duration::from_secs(0)),
            interval_s: None,
            timeout_s: None,
            stdout: Some(StdoutNotifierConfig::example()),
            discord: Some(DiscordNotifierConfig::example()),
            opsgenie: None, // TODO: example config
            slack: Some(SlackConfig::example()),
            webex: Some(WebexNotifierConfig::example()),
        }
    }
}
