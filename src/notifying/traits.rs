/*! Traits for the notifiers.
*/
use async_trait::async_trait;

use crate::polling::config::{HelzCheckError, HelzCheckStatus};

#[async_trait]
/// The trait all notifiers have to implement.
pub trait Notifier {
    /// Called when an error is to be reported.
    async fn notify(&self, err: &[HelzCheckStatus], whoami: &str);
    /// Called when an error went back to being OK.
    async fn ok(&self, erors: &[HelzCheckStatus], whoami: &str);
}

/// Renders a detailed error message using markdown.
pub fn error_message_detailed(errors: &[HelzCheckStatus], whoami: &str) -> String {
    let first_error = &errors[0];
    let last_error = &errors[errors.len() - 1];

    let unique_error_text = unique_errors_output(errors);

    format!(
        r#"
The service `{}` has been detected as **down** by `{}` ❌.

First occurence: {} ({}s ago)
Last occurence: {} ({}s ago)

Unique errors:
{}
"#,
        first_error.name,
        whoami,
        first_error
            .human_time
            .to_rfc3339_opts(chrono::SecondsFormat::Secs, true),
        first_error.time.elapsed().as_secs(),
        last_error
            .human_time
            .to_rfc3339_opts(chrono::SecondsFormat::Secs, true),
        last_error.time.elapsed().as_secs(),
        unique_error_text,
    )
}

/// Finds the unique errors in a list of errors.
pub fn unique_errors(errors: &[HelzCheckStatus]) -> Vec<HelzCheckError> {
    let mut unique_errors_types: Vec<HelzCheckError> = Vec::new();
    let mut unique_errors: Vec<HelzCheckStatus> = Vec::new();

    for err in errors {
        if !unique_errors_types.contains(&err.response) {
            unique_errors_types.push(err.response.to_owned());
            unique_errors.push(err.to_owned());
        }
    }

    unique_errors_types
}

/// Generate output for the unique errors.
pub fn unique_errors_output(errors: &[HelzCheckStatus]) -> String {
    let mut unique_error_output = "".to_owned();
    for err in unique_errors(errors) {
        let out = format!("\n- {err}");
        unique_error_output += &out;
    }

    unique_error_output
}

/// Renders a detailed OK message using markdown.
pub fn ok_message(errors: &[HelzCheckStatus], whoami: &str) -> String {
    let first_error = &errors[0];
    let last_error = &errors[errors.len() - 1];

    let unique_errors_text = unique_errors_output(errors);

    let message = format!(
        r#"
The service `{}` has been detected as **up** by `{}` ✅.

First occurence: {} ({}s ago)
Last occurence: {} ({}s ago)

Errors seen:
{}
"#,
        first_error.name,
        whoami,
        first_error
            .human_time
            .to_rfc3339_opts(chrono::SecondsFormat::Secs, true),
        first_error.time.elapsed().as_secs(),
        last_error
            .human_time
            .to_rfc3339_opts(chrono::SecondsFormat::Secs, true),
        last_error.time.elapsed().as_secs(),
        unique_errors_text,
    );

    message
}
