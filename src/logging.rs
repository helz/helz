/*!
*/

use env_logger::Builder;
use log::LevelFilter;

fn get_log_level(level: &str) -> LevelFilter {
    match level {
        "t" | "trace" => LevelFilter::Trace,
        "d" | "debug" | "v" | "verbose" => LevelFilter::Debug,
        "i" | "info" => LevelFilter::Info,
        "w" | "warn" | "warning" => LevelFilter::Warn,
        "e" | "error" => LevelFilter::Error,
        _ => {
            warn!("unparseable log level '{}', defaulting to Warn", level);
            LevelFilter::Warn
        }
    }
}

pub fn init_logger(loglevel: Option<String>) {
    let mut logger_builder = Builder::from_env(env_logger::Env::default().default_filter_or("helz=info,warn"));

    if let Some(str_log_level) = loglevel {
        let log_level = get_log_level(&str_log_level);
        logger_builder.filter_level(log_level);
    }

    logger_builder.init()
}
