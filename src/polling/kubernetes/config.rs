/*! Kubernetes Poller Config
 */
use serde::{Deserialize, Serialize};

use crate::config::traits::Example;

use crate::polling::kubernetes::types::{Scope, TrackedResourceWithStatus, DEFAULT_ANNOTATION};

/// Configuration for the Kubernetes poller.
/// By default the poller will auto-configure itself based on the
/// KUBECONFIG environment variable or in-cluster environment variables
/// (if a ServiceAccount is provisioned and a token is available).
///
/// The values for what the poller will look for can be configured in TOML as follows:
/// ```
/// # use libhelz::polling::kubernetes::KubernetesConfig;
/// # let conf: KubernetesConfig = toml::from_str(r#"
/// namespace = "default"
/// deployment = "foo"
/// # "#).expect("example configuration should work");
/// ```
/// It might be possible to connect to a cluster using only
/// URL. It is preferred to infer the values from the env.
/// ```
/// # use libhelz::polling::kubernetes::KubernetesConfig;
/// # let conf: KubernetesConfig = toml::from_str(r#"
/// namespace = "default"
/// deployment = "foo"
/// [client_config]
/// cluster_url = "https://cluster.example.org"
/// timeout = "10s"
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example:
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// [polling.kubernetes]
/// namespace = "default"
/// deployment = "foo"
/// [polling.notifier.stdout]
/// prefix = "kubernetes-error"
/// # "#)
/// # .expect("example configuration should work");
/// ```
#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
pub struct KubernetesConfig {
    /// Provide static configuration for the Kubernetes API client.
    /// If left empty, Helz will attempt to configure the client dynamically
    /// by using e.g. KUBECONFIG.
    pub client_config: Option<KubeConfig>,
    /// Namespace to run poller in.
    pub namespace: Scope,
    /// Name of deployment to monitor.
    pub deployment: Option<String>,
    /// Look for annotations
    pub auto: Option<bool>,
    /// Annotation to look for
    pub annotation: Option<String>,
    /// Create generic checks of the .status field on a kubernetes resource.
    pub resources: Option<Vec<TrackedResourceWithStatus>>,
}

impl Example for KubernetesConfig {
    fn example() -> Self {
        Self {
            annotation: Some(DEFAULT_ANNOTATION.to_string()),
            client_config: None,
            namespace: Scope::Namespaced("default".to_string()),
            deployment: None,
            auto: Some(true),
            resources: Some(vec![TrackedResourceWithStatus::example()]),
        }
    }
}

/// Configuration to pass when manually creating a kubernetes client.
// TODO: Figure out how to implement all kinds of manual auths
// other than the async ones reading from files/env/+++.
// Want to be able to set this statically 🤔
#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
pub struct KubeConfig {
    /// URL the cluster.
    pub cluster_url: crate::types::uri::Uri,
    // TODO: Figure out how to implement this.
    // root_cert: Option<Vec<Vec<u8, alloc::alloc::Global>, alloc::alloc::Global>>,
    /// Timeout for cluster API calls.
    #[serde(default)]
    #[serde(with = "humantime_serde")]
    pub timeout: Option<std::time::Duration>,
    /// Whether to accept invalid certs or not.
    pub accept_invalid_certs: Option<bool>,
}
