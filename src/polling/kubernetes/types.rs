/*! Kubernetes Poller Types
 */
use serde::{Deserialize, Serialize};
use std::time;
use url::Url;

use crate::config::traits::Example;

/// Default Kubernetes annotation for enabling Helz watch in auto mode
/// Must be set to "enabled" for Helz to configure a poller for the resource.
/// Otherwise, it is treated as being disabled.
pub const DEFAULT_ANNOTATION: &str = "helz.sklirg.io/watch";
const ANNOTATION_NAME: &str = "helz.sklirg.io/name";
const ANNOTATION_TIMEOUT: &str = "helz.sklirg.io/timeout";
const ANNOTATION_INTERVAL: &str = "helz.sklirg.io/interval";
const ANNOTATION_NOTIFY_SLACK: &str = "alpha.helz.sklirg.io/slack-webhook-url";

/// Update internal Helz state of Kubernetes objects it has seen.
#[derive(Debug)]
pub enum TrackedObject {
    /// Add a deployment
    AddDeployment(HelzKubernetesResource),
    /// Remove a deployment
    RemoveDeployment(KubernetesResourceKey),
    /// Add a Kubernetes Resource
    Add(HelzKubernetesResource),
    /// Remove a Kubernetes Resource
    Remove(KubernetesResourceKey),
}

impl std::fmt::Display for TrackedObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let (typ, name) = match self {
            TrackedObject::AddDeployment(resource) => ("AddDeployment", resource.to_string()),
            TrackedObject::RemoveDeployment(name) => ("RemoveDeployment", name.to_string()),
            TrackedObject::Add(key) => ("Add", key.to_string()),
            TrackedObject::Remove(key) => ("Remove", key.to_string()),
        };

        write!(f, "KubernetesPoller::{typ}={name}")
    }
}

/// Type alias for simplification of the sender for TrackedObjects.
pub type TrackedObjectsSender = tokio::sync::mpsc::UnboundedSender<TrackedObject>;

/// A type-safe key for Kubernetes Resources watched by Helz. Used
/// to index a HashMap<Key, Resource>
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct KubernetesResourceKey(String);

impl KubernetesResourceKey {
    /// Create a Key manually by supplying resource namespace and name
    pub fn new(gvk: &str, namespace: &str, name: &str) -> Self {
        Self(format!("{gvk}/{namespace}/{name}"))
    }
}

impl std::fmt::Display for KubernetesResourceKey {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Scope of the Kubernetes Resource, either Cluster or Namespaced.
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
#[serde(untagged)]
pub enum Scope {
    /// Cluster-wide resource
    Cluster(bool),
    /// Namespaced resource
    Namespaced(String),
}

impl std::fmt::Display for Scope {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let msg = match self {
            Self::Cluster(_) => "cluster",
            Self::Namespaced(ns) => ns,
        };
        write!(f, "{msg}")
    }
}

/// A Helz-native object to keep track of required configuration
/// from a Kubernetes object.
#[derive(Clone, Debug)]
pub struct HelzKubernetesResource {
    /// Group of Group/Version/Kind
    pub group: String,
    /// Version of Group/Version/Kind
    pub version: String,
    /// Kind of Group/Version/Kind
    pub kind: String,
    /// The namespace the object lives in, or cluster-wide
    pub namespace: Scope,
    /// Name of object
    pub name: String,
    /// Slack webhook to post notifications to, if configured
    pub slack_webhook_url: Option<url::Url>,
    /// How long to wait before first notify
    pub timeout: time::Duration,
    /// How often to notify
    pub interval: time::Duration,
}

impl HelzKubernetesResource {
    /// Convenience function to creat a Helz-native Kubernetes resource
    pub fn new(
        group: String,
        version: String,
        kind: String,
        namespace: Scope,
        name: String,
    ) -> Self {
        Self {
            group,
            version,
            kind,
            namespace,
            name,
            interval: time::Duration::from_secs(5),
            timeout: time::Duration::from_secs(0),
            slack_webhook_url: None,
        }
    }

    /// Generate the GVK for this resource.
    pub fn gvk(&self) -> String {
        format!("{}/{}/{}", self.group, self.version, self.kind)
    }

    /// Creat a full Helz-native Kubernetes resource from a Kubernetes resource
    pub fn from_kubernetes<R>(resource: R, annotation: &str) -> Option<Self>
    where
        R: kube::core::Resource,
    {
        let metadata = resource.meta();
        if let Some(annotations) = metadata.clone().annotations {
            if let Some(value) = annotations.get(annotation) {
                if value != "enabled" {
                    debug!(
                        "Found helz annotation, but it is not 'enabled' (was '{}'). Skipping.",
                        value
                    );
                    return None;
                } else {
                    let mut conf = Self::new(
                        // TODO: resource.group(), resource.version(), resource.kind());
                        "apps".to_owned(),
                        "v1".to_owned(),
                        "Deployment".to_owned(),
                        Scope::Namespaced(metadata.namespace.clone().unwrap()),
                        metadata.name.clone().unwrap(),
                    );
                    if let Some(name) = annotations.get(ANNOTATION_NAME) {
                        conf.name = name.to_owned();
                    }
                    if let Some(url) = annotations.get(ANNOTATION_NOTIFY_SLACK) {
                        match Url::parse(url) {
                            Ok(url) => conf.slack_webhook_url = Some(url),
                            Err(e) => warn!("Tried to parse {url} as URL but failed: {e}"),
                        };
                    }
                    if let Some(raw) = annotations.get(ANNOTATION_INTERVAL) {
                        if let Ok(duration) = humantime::parse_duration(raw) {
                            conf.interval = duration;
                        } else {
                            warn!("Failed to parse {raw} as duration");
                        }
                    }
                    if let Some(raw) = annotations.get(ANNOTATION_TIMEOUT) {
                        if let Ok(duration) = humantime::parse_duration(raw) {
                            conf.timeout = duration;
                        } else {
                            warn!("Failed to parse {raw} as duration");
                        }
                    }
                    return Some(conf);
                }
            } else {
                debug!(
                    "Skipping {} because it doesn't contain helz annotation {}",
                    metadata.name.clone().unwrap(),
                    annotation
                );
                return None;
            }
        }
        None
    }

    /// Create a KubernetesResourceKey from a Kubernetes resource.
    pub fn key(&self) -> KubernetesResourceKey {
        KubernetesResourceKey::new(&self.gvk(), &self.namespace.to_string(), &self.name)
    }
}

impl std::fmt::Display for HelzKubernetesResource {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "K8s:{}_{}/{}", self.gvk(), self.namespace, self.name)
    }
}

/// Configure a generic check of the status field of a Kubernetes object.
#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
pub struct TrackedResourceWithStatus {
    /// GroupVersionKind (e.g. apps/v1/Deployment or /v1/Pod)
    pub gvk: String,
    /// List of checks that have to succeed for the resource to be considered healthy.
    pub conditions: Vec<Condition>,
}

/// Contains the configuration fields of a condition.
#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
pub struct Condition {
    /// Type of condition, e.g. Ready
    #[serde(rename = "type")]
    pub type_: String,
    /// Status of condition, e.g. True
    pub status: Vec<String>,
    /// Reason will be set to the reason of a failing Condition, if available.
    pub reason: Option<String>,
}

impl Example for TrackedResourceWithStatus {
    fn example() -> Self {
        Self {
            gvk: "apps/v1/Deployment".to_owned(),
            conditions: vec![Condition::example()],
        }
    }
}

impl Example for Condition {
    fn example() -> Self {
        Self {
            type_: "Available".to_owned(),
            status: vec!["True".to_owned()],
            reason: None,
        }
    }
}
