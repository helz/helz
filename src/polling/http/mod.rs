/*! Polling over HTTP
 */

pub mod config;
pub mod main;
pub mod types;

pub use self::config::HttpPollingConfig;
pub use self::main::HttpPolling;
