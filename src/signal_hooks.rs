/*!
*/

use futures::StreamExt;

#[derive(Clone, Copy)]
pub enum SignalEvent {
    Signal(i32),
    Stop,
}

pub async fn watcher(
    mut signals: signal_hook_tokio::Signals,
    tx: tokio::sync::watch::Sender<SignalEvent>,
) {
    debug!("starting watching for unix signals");
    let mut v = 0;
    loop {
        while let Some(signal) = signals.next().await {
            match signal {
                signal_hook::consts::signal::SIGHUP => {
                    debug!("sighup on config v{}, bumping to v{}", v, v + 1);
                    v += 1;
                    tx.send_replace(SignalEvent::Signal(v));
                }
                signal_hook::consts::signal::SIGINT => {
                    info!("Received SIGINT, shutting down.");
                    tx.send_replace(SignalEvent::Stop);
                    return;
                }
                _ => {
                    warn!("unhandled signal");
                }
            }
        }
    }
}
