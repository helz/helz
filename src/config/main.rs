/*! Main source of Helz configuration
*/

use serde::{Deserialize, Serialize};
use toml;

use crate::config::traits::Example;
use crate::polling::config::PollingConfig;

#[derive(Clone, Deserialize, Debug, Serialize, Eq, PartialEq)]
/// The global configuration of Helz.
/// Usually parsed as TOML.
pub struct Config {
    /// Name of this host. Defaults to hostname.
    pub name: Option<String>,
    /// URL to Open Telemetry Collector Agent
    pub open_telemetry_agent_url: Option<url::Url>,
    /// A list of pollers to configure.
    pub polling: Vec<PollingConfig>,
}

impl Example for Config {
    fn example() -> Self {
        Config {
            polling: vec![PollingConfig::example()],
            name: Some(
                gethostname::gethostname()
                    .into_string()
                    .unwrap_or_else(|_| "HostName".to_owned()),
            ),
            open_telemetry_agent_url: Some(url::Url::parse("http://localhost:4318").unwrap()),
        }
    }
}

/// Parse some TOML configuration
pub fn configure(conf: &str) -> Result<Config, ConfigError> {
    let mut toml_config: Config = match toml::from_str(conf) {
        Ok(conf) => conf,
        Err(err) => {
            error!("toml parse error: {}", err);
            return Err(ConfigError::TomlError(err.to_string()));
        }
    };

    if toml_config.name.is_none() {
        toml_config.name = Some(
            gethostname::gethostname()
                .into_string()
                .expect("failed to get hostname for host"),
        );
    }

    Ok(toml_config)
}

#[derive(Debug)]
/// The variants a configuration can fail with.
pub enum ConfigError {
    /// Returned if the configuration failed to parse.
    ParseError,
    /// Returned if we had problems reading configuration from a file.
    IOError,
    /// Returned if the TOML parser failed.
    TomlError(String),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_config_to_toml() {
        let conf = Config::example();
        toml::to_string(&conf).unwrap();
    }
}
