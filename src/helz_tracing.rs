pub fn configure_open_telemetry() {
    use opentelemetry_otlp::WithExportConfig;
    let mut tracer = opentelemetry_otlp::new_pipeline().tracing();

    let otel_url = match std::env::var(opentelemetry_otlp::OTEL_EXPORTER_OTLP_ENDPOINT) {
        Ok(url) => Some(url),
        Err(_) => None,
    };

    if let Some(otel_url) = otel_url {
        tracer = match url::Url::parse(&otel_url) {
            Ok(_) => tracer
                .with_exporter(opentelemetry_otlp::new_exporter().tonic().with_env())
                .with_trace_config(
                    opentelemetry::sdk::trace::config()
                        .with_sampler(opentelemetry::sdk::trace::Sampler::AlwaysOn)
                        .with_max_events_per_span(64)
                        .with_max_attributes_per_span(16)
                        .with_resource(opentelemetry::sdk::resource::Resource::new(vec![
                            opentelemetry::KeyValue::new("service.name", "helz"),
                        ])),
                ),
            Err(e) => {
                println!("Attempted to configure OpenTelemetry, but failed to parse URL: {e}. Will run Helz without OTel or logging!");
                return;
            }
        };
    }

    let tracer = match tracer.install_batch(opentelemetry::runtime::Tokio) {
        Ok(tracer) => tracer,
        Err(err) => {
            println!("Failed to configure OpenTelemetry/logging! (error: {err})");
            return;
        }
    };

    let opentelemetry = tracing_opentelemetry::layer().with_tracer(tracer);

    use tracing_subscriber::Layer;
    let filter = tracing_subscriber::EnvFilter::from_default_env();
    let stdout = tracing_subscriber::fmt::layer().with_filter(filter);

    use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};
    match tracing_subscriber::registry()
        .with(stdout)
        .with(opentelemetry)
        .try_init() {
            Ok(_) => (),
            Err(err) => println!("Failed to initialize tracing and logging, but failed. No log output will be shown! (error: {err})"),
        }
}
